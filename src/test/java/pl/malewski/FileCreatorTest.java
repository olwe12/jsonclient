package pl.malewski;

import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

import static org.junit.jupiter.api.Assertions.*;

class FileCreatorTest {

    private static final String fileSeparator = FileSystems.getDefault().getSeparator();
    private static final String address = "file:src" + fileSeparator + "test" + fileSeparator +
            "resources" + fileSeparator + "json"
            + fileSeparator + "jsonData.json";

    @Test
    void fileCreatorShouldCreateEightsFile() throws IOException {
        //given
        long numberOfFiles = 0;
        long expectedNumberOfFiles = 8;

        //when
        new FileCreator().createFiles(address);
        try (Stream<Path> files = Files.list(Paths.get("."))) {
            numberOfFiles = files.filter(f -> f.toString().endsWith(".json")).count();
        }
        //then
        assertEquals(expectedNumberOfFiles, numberOfFiles);
    }
}
