package pl.malewski;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;

public class DataLoader {
    StringBuffer loadFrom(String address) throws IOException {
        StringBuffer stringBuffer = new StringBuffer();
        URL jsonPlaceholder = new URL(address);
        BufferedReader in = new BufferedReader(
                new InputStreamReader(jsonPlaceholder.openStream()));
        String inputLine;
        while ((inputLine = in.readLine()) != null) {
            stringBuffer.append(inputLine);
        }
        in.close();
        return stringBuffer;
    }
}
