package pl.malewski;

import java.io.IOException;

public class Main {

    private static final String ADDRESS = "https://jsonplaceholder.typicode.com/posts";

    public static void main(String[] args) {
        try {
            new FileCreator().createFiles(ADDRESS);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
