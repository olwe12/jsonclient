package pl.malewski;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class FileCreator {

    private final static int THREADS_NUMBER = 15;
    private final ExecutorService executorService = Executors.newFixedThreadPool(THREADS_NUMBER);

    public void createFiles(String address) throws IOException {
        StringBuffer stringBuffer = new DataLoader().loadFrom(address);
        JsonArray asJsonArray = parseData(stringBuffer.toString());
        writeDataToFiles(asJsonArray);
        executorService.shutdown();
    }

    private JsonArray parseData(String json) {
        return JsonParser.parseString(json).getAsJsonArray();
    }

    private void writeToFile(String filename, JsonObject post) throws IOException {
        BufferedWriter writer = new BufferedWriter(new FileWriter(filename, false));
        writer.write(post.toString());
        writer.close();
    }

    private void writeDataToFiles(JsonArray asJsonArray) {
        asJsonArray.forEach(json -> executorService.submit(() -> {
            try {
                writeToFile(json.getAsJsonObject().get("id").toString() + ".json", json.getAsJsonObject());
            } catch (IOException e) {
                e.printStackTrace();
            }
        }));
    }
}
